#Beningreenjam/ApiWrappper for Laravel 5.5+

##**WARNING**
This library is in beta and should not be relied upon for production use. Use at your own risk.

A simple api wrapper with quick and easy caching and throttling.

##Configuration

To configure a new api key you must register it in your config/services.php where in the endpoint variable `example.endpoint` is the is the base URI used when calling the API in question.

###Required

	[
		'example' => [
			'endpoint' => 'http://api.example.com/'
		]
	]

##Usage

Example to resolve http://api.example.com/baz

###Required

	$api = new Beningreenjam\Api\ApiWrapper();
	$api->config('example');
	$api->path('baz');
	$result = $api->request()->result();
	
###Query Parameters
If you need to perform a query using HTTP GET key-value pairs, then you will need to use the `setQueryParameters( $parameters )` method. This method takes one argument which must be a flat associative array, with string variables.

__The `setQueryParameters` method must be called before the `request` method.

	$api = new Beningreenjam\Api\ApiWrapper();
	$api->config('example');
	$api->path('baz');
	$api->setQueryParameters([
	    'foo'    => 'bar',
	    'boolean => 'true',
	    'number' => '1'
	]);
	$result = $api->request()->result();
	
###Caching

If you want to cache the response. Use the `store()` method. The response will only ever be cached when the response of the API request has a **200** status code.

	$api->store();
	
If you want to store a transformed version of the response, then you may pass in the transformed data as an optional parameter.

	$api->store($transformed);
	
You adjust the time that the response is cached by changing the `cache_time` property in minutes, which defaults to `1440` (24 hours). Whatever value you set it to, it must be an int with a value greater than zero.

###Throttling

Throttling is enabled by default and has a limit of 100 requests per hour. However you can easily increase or decrease the `throttle_limit` and `throttle_time` properties to your desired values. To disable throttling, simply enter `0` for both properties.

You can query the remaining calls to the API via the `queriesRemaining()` method.

##Error handling

This library works by throwing exceptions which you would need to catch.

* Invalid configuration: `ConfigException`
* Throttled API: `ThrottleException`
* Request error: `RequestException`
* Response error: `ResponseException`
* Server error: `ServerException`