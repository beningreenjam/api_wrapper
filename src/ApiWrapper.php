<?php namespace Beningreenjam\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Beningreenjam\Api\Exception\ThrottleException;
use Beningreenjam\Api\Exception\ConfigException;
use Beningreenjam\Api\Exception\RequestException;
use Beningreenjam\Api\Exception\ResponseException;
use Beningreenjam\Api\Exception\ServerException;
use Cache;

class ApiWrapper {
    public $throttle_limit = 100;
    public $throttle_time  = 60;
    public $cache_time     = 1440;

    private $method  = 'GET';
    private $cache_key;
    private $api_endpoint;
    private $path;
    private $queries_key;
    private $options = [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ];
    private $result;

    public function config( $api_name ) {
        if( ! is_string( $api_name ) ) {
            throw new ConfigException(
                class_basename( $this ).' expects $api_name parameter to be a string. '.gettype( $api_name ).' given.'
            );
        }

        $api_name = snake_case( $api_name );

        $this->api_endpoint = config( "services.{$api_name}.endpoint" );
        $this->queries_key = "{$api_name}_count";

        if( ! isset( $this->api_endpoint ) || empty( $this->api_endpoint ) || ! is_string( $this->api_endpoint ) ) {
            throw new ConfigException(
                "Unable to find {$api_name} endpoint. services.{$api_name}.endpoint, defined in ".
                config_path().
                "services.php, must be a publicly accessible URL."
            );
        }

        return $this;
    }

    public function path( $path ) {
        $this->path = (string) $path;

        return $this;
    }

    public function setQueryParameters( $parameters ) {
        array_set( $this->options, 'query', (array) $parameters );

        return $this;
    }

    public function setXml( $xml ) {
        return $this->setBody( $xml, 'application/xml' );
    }

    public function setJson( $json ) {
        return $this->setBody( $json );
    }

    public function setBody( $body, $contentType = 'application/json' ) {
        $this->setHeader( 'Content-type', $contentType );

        array_set( $this->options, 'body', $body );

        return $this;
    }

    public function request( $method = 'GET' ) {
        $search = $this->api_endpoint.$this->path;

        $this->cache_key = $this->cacheKey( $search );
        $this->method = $method;

        $this->result = Cache::get(
            $this->cache_key,
            function () {
                if( $this->throttle_limit > 0 && $this->throttle_time > 0 ) {
                    $this->throttleCheck();
                }

                return $this->queryAPI();
            }
        );

        return $this;
    }

    public function store( $transformed = null ) {
        if( $this->status() == 200 ) {
            if(is_null( $transformed)) {
                Cache::put( $this->cache_key, $this->result(), $this->cache_time );
            }
            else {
                Cache::put( $this->cache_key, $transformed, $this->cache_time );
            }
        }

        return $this;
    }

    public function getHeader( $headerName ) {
        if( $this->result instanceof Response ) {
            return $this->result->getHeaderLine( $headerName );
        }

        return 'X-Source cache';
    }

    public function getHeaders() {
        if( $this->isResponse() ) {
            return $this->result->getHeaders();
        }

        return [ 'X-Source' => 'cache' ];
    }

    public function setHeader( $key, $value ) {
        array_set( $this->options, "headers.{$key}", $value );

        return $this;
    }

    public function setHeaders( $headers ) {
        array_set( $this->options, 'headers', array_merge( array_get( $this->options, 'headers', $headers ) ) );

        return $this;
    }

    public function clearHeaders() {
        array_set( $this->options, 'headers', [ ] );

        return $this;
    }

    public function status() {
        if( $this->isResponse() ) {
            return $this->result->getStatusCode();
        }

        return 200;
    }

    public function responsePhrase() {
        if( $this->isResponse() ) {
            return $this->result->getReasonPhrase();
        }

        return 'OK';
    }

    public function fromEndpoint() {
        return is_null( array_get( $this->getHeaders(), 'X-Source' ) );
    }

    public function queriesRemaining() {
        return $this->throttle_limit - $this->queries();
    }

    public function result() {
        if( $this->isResponse() ) {
            return $this->transformType( $this->result->getBody()->getContents() );
        }

        return $this->result;
    }

    private function isResponse() {
        return $this->result instanceof \GuzzleHttp\Psr7\Response;
    }

    private function queryAPI() {
        try {
            $client = new Client();

            return $client->request( $this->method, $this->api_endpoint.$this->path, $this->options );
        }
        catch( \GuzzleHttp\Exception\ServerException $e ) {
            throw new ServerException( $e->getMessage(), $e->getCode() );
        }
        catch( \GuzzleHttp\Exception\ClientException $e ) {
            throw new ResponseException( $e->getMessage(), $e->getCode() );
        }
        catch( \GuzzleHttp\Exception\BadResponseException $e ) {
            throw new ResponseException( $e->getMessage(), $e->getCode() );
        }
        catch( \GuzzleHttp\Exception\RequestException $e ) {
            throw new RequestException( $e->getMessage(), $e->getCode() );
        }
    }

    private function throttleCheck() {
        if( $this->throttle_limit <= $this->queries() ) {
            throw new ThrottleException( 'API throttled due to flood of requests', 428 );
        }

        if( ! Cache::has( $this->queries_key ) ) {
            Cache::put( $this->queries_key, 0, $this->cache_time );
        }

        Cache::increment( $this->queries_key );
    }

    private function cacheKey( $search ) {
        return md5( $this->api_endpoint.serialize( [ $search, $this->options ] ) );
    }

    private function transformType( $result ) {
        switch( $this->getHeader( 'content-type' ) ) {
            case 'application/json':
            case 'application/json; charset=utf-8':
                return json_decode( $result, true );
                break;
            case 'application/xml':
            case 'text/xml':
                return json_decode( json_encode( $result ), true );
                break;
            default:
                return $result;
        }
    }

    private function queries() {
        return Cache::get( $this->queries_key, 0 );
    }
}